#include "Matrix.h"

using namespace std;

int main(){

	cout << endl;
	cout << "creating matrices" << endl;
	Matrix first_matrix(2,3);
	Matrix second_matrix(3,2);
	Matrix third_matrix(3,3);
	Matrix fourth_matrix(3,3);
	
	cout << endl;
	cout << "size of first matrix = " << first_matrix.size() << endl;
	cout << "size of second matrix = " << second_matrix.size() << endl;
	cout << "size of third matrix = " << third_matrix.size() << endl;
	cout << "size of fourth matrix = " << fourth_matrix.size() << endl;
 
	//set matrix values to random numbers between 0 and 10
	for (int i=0; i<first_matrix.rows(); i++){
		for (int j=0; j<first_matrix.cols(); j++)
			first_matrix.set(i,j,drand48()*10);  //personally I prefer when these are integers, easier to check output
	}
	for (int i=0; i<second_matrix.rows(); i++){
		for (int j=0; j<second_matrix.cols(); j++)
			second_matrix.set(i,j,drand48()*10);
	}

	for (int i=0; i<third_matrix.rows(); i++){
		for (int j=0; j<third_matrix.cols(); j++)
			third_matrix.set(i,j,drand48()*10);
	}
	for (int i=0; i<fourth_matrix.rows(); i++){
		for (int j=0; j<fourth_matrix.cols(); j++)
			fourth_matrix.set(i,j,drand48()*10);
	}

	cout << endl;
	cout << "testing std::ostream operator<<(...) by printing first and second matrices:" << endl;
	cout << "first matrix:" << endl << first_matrix;
	cout << "second matrix:" << endl << second_matrix << endl;	
	
	cout << "testing print() method by printing out third and fourth matrices" << endl;
	cout << "third matrix:" << endl;
	third_matrix.print();
	cout << "fourth matrix:" << endl;
	fourth_matrix.print();

	cout << endl;
	cout << "testing get method" << endl;
	double a = first_matrix.get(0,0);
	double b = second_matrix.get(second_matrix.rows()-1,second_matrix.cols()-1);
	cout << "the first entry of the first matrix is " << a << " and the last entry of the second matrix is " << b << endl;

	cout << endl;
	cout << "testing overloaded [] and () operators" << endl;
	double c = first_matrix[0][0];
	double d = second_matrix(second_matrix.rows()-1, second_matrix.cols()-1);
	cout << "the first entry of the first matrix is " << c << " and the last entry of the second matrix is " << d << endl;
	cout << endl;

	cout << "testing overloaded assignment operator" << endl;
	Matrix first_copy(first_matrix.rows(), first_matrix.cols());
	first_copy = first_matrix;
	cout << "first copy:" << endl << first_copy << endl;

	cout << "testing overloaded addition operator + " << endl;
	Matrix added_matrix(third_matrix.rows(), third_matrix.cols());
	added_matrix = (third_matrix + fourth_matrix);
	cout << "The sum of the third and fourth matrices is:" << endl << added_matrix << endl;

	cout << "testing overloaded subtraction operator -" << endl;
	Matrix sub_matrix(third_matrix.rows(), third_matrix.cols());
	sub_matrix = (third_matrix - fourth_matrix);
	cout << "The fourth matrix subtracted from the third is:" << endl << sub_matrix << endl;

	cout << "testing overloaded += operator " << endl;
	third_matrix += fourth_matrix;
	cout << "third matrix after doing 'third_matrix += fourth_matrix': " << endl << third_matrix << endl;

	cout << "testing overloaded -= operator " << endl;
	third_matrix -= fourth_matrix;
	cout << "third matrix after doing 'third_matrix -= fourth_matrix': " << endl << third_matrix << endl;

	cout << "testing overloaded != and == operators" << endl;
	if (first_matrix != first_matrix)
		cout << "first matrix is not equal to itself" << endl;
	if (first_matrix == first_matrix)
		cout << "the first matrix is equal to itself" << endl;
	if (first_matrix != second_matrix)
		cout << "the first matrix is not equal to the second matrix " << endl;
	if (first_matrix == second_matrix)
		cout << "the first matrix is equal to the second matrix " << endl;

	cout << endl;
	cout << "testing transpose method" << endl;
	Matrix T(first_matrix.cols(), first_matrix.rows());
	T = first_matrix.transpose();
	cout << "tranpose of first_matrix:" << endl;
	cout << T << endl;

	cout << "testing matrix multiplication function" << endl;
	Matrix prod_matrix(first_matrix.rows(), T.cols());
	prod_matrix = mat_mul(first_matrix, T);
	cout << "the first matrix multiplied by its transpose:" << endl;
	cout << prod_matrix;

	//prod_matrix = mat_mul(first_matrix, second_matrix); //this shouldn't work

	return 0;
}
