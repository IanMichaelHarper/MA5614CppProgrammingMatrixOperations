CC = g++ -std=c++11
CFLAGS = -Wall -g

OBJECTS = Matrix.o

target: testMatrix.cc Matrix.h $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) testMatrix.cc -o Matrix 

test: testMatrix.cc Matrix.h $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) testMatrix.cc -o Matrix
	./Matrix

Matrix.o: Matrix.h Matrix.cc
	$(CC) $(CFLAGS) -c Matrix.cc 
	
clean:
	 rm Matrix *.o
