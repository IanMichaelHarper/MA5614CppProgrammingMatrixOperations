#include<iostream>

using namespace std;

class Matrix { 

private:

	int nrows; 		 // the number of rows
	int ncols;		 // the number of columns
	int sz;			 // the number of elements in the matrix
	double* elem;		 // a pointer to the elements

public:

	//constructor
	Matrix(int nrows, int ncols);

	//deep copy constructor
	Matrix(const Matrix& mat_in);

	// initialiser list constructor
	Matrix(std::initializer_list<double> lst); 
	
	//deep assignment operator
	Matrix& operator=(const Matrix& mat_in);
	
	//destructor
	~Matrix();

	//overloaded operators to access elements of matrix
	double* operator[](int r);
	double operator()(const int i, const int j);
	
	//overloaded operators for += and -=
	void operator+=(Matrix & mat_a);
	void operator-=(Matrix & mat_a);

	//methods to set and get matrix elements
	void set(const int i, const int j, const double x);
	double get(const int i, const int j);

	int size();  //returns the number of elements in the matrix
	int rows();  //returns the number of rows of the matrix
	int cols();  //returns the number of columns of the matrix

	Matrix transpose();  //tranposes matrix

	void print();  //prints matrix
};

//overloadedarithmetic operators - outside class
Matrix operator+(Matrix& mat_a, Matrix& mat_b);
Matrix operator-(Matrix& mat_a, Matrix& mat_b);

//overloaded boolean operators
bool operator!=(Matrix& mat_a, Matrix& mat_b);
bool operator==(Matrix& mat_a, Matrix& mat_b);

//matrix multiplication
Matrix mat_mul(Matrix& mat_a, Matrix& mat_b);

//output
std::ostream& operator<<(std::ostream &os, Matrix& mat_in);
