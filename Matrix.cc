#include "Matrix.h"

//constructor
Matrix::Matrix(int nrows, int ncols) : nrows{nrows}, ncols{ncols}, sz{nrows*ncols}, elem{new double[sz]}{
	std::cout << "using regular constructor - with \'new\'" << std::endl;
		
	//... initialise elements to zero?
	for (int i=0; i<sz; i++){
		elem[i]=0.0;
	}
};

//deep copy constructor
Matrix::Matrix(const Matrix& mat_in) : sz(mat_in.sz)
{
	std::cout << "using deep copy constructor" << std::endl;
	if (mat_in.elem != 0) {
		elem = new double[sz]; 
		for (int i = 0;i<sz;i++){ elem[i]=mat_in.elem[i]; }
	} else {
		elem = 0;
	}
}

//initialise using a list
Matrix::Matrix(std::initializer_list<double> lst) : sz{int(lst.size())}, elem{new double[sz]}
{
	std::copy( lst.begin(),lst.end(),elem);
}

//deep assignment operator
Matrix& Matrix::operator=(const Matrix& mat_in) {
	std::cout << "using deep assignment operator" << std::endl;
	if (this == &mat_in){ return *this; }
		
	double* p = new double[mat_in.sz]; //allocate new memory
	std::copy(mat_in.elem, mat_in.elem + mat_in.sz, p); //does the copy

	delete[] this->elem;
		
	elem = p; //reassign internal memory
	sz = mat_in.sz; //set internal size
		
	return *this;
}

// destructor
Matrix::~Matrix(){				 
	delete[] elem;	 // deallocates memory
}

//overloaded [] operator
double* Matrix::operator[](const int r){ 
	return elem + r*ncols;
}

//overloaded () operator
double Matrix::operator()(const int i, const int j){ //access (i,j)th element
	return elem[i*ncols + j];
}

//set method to set matrix entries
void Matrix::set(const int i, const int j, const double x){ //change (i,j)th element
	elem[i*this->ncols + j]=x;
}

//get method to get matrix entries
double Matrix::get(const int i, const int j){  //access (i,j)th element
	return elem[i*this->ncols + j];
}

//returns number of elements in matrix
int Matrix::size(){
	return sz;
}

//returns the number of rows of matrix
int Matrix::rows(){
	return nrows;
}

//returns the number of columns of matrix
int Matrix::cols(){
	return ncols;
}

//do some output
void Matrix::print(){
	for (int i = 0; i<nrows; ++i){
		std::cout << "(";
		for (int j = 0; j<ncols; ++j)
			std::cout << elem[i*ncols + j] << " ";
		std::cout << "\b)" << std::endl;
	}
}

//tranpose method
Matrix Matrix::transpose(){
	Matrix T(this->ncols, this->nrows);
	for(int i=0; i<this->nrows;i++){
		for (int j=0; j<this->ncols; j++){
			T[j][i] = this->elem[i*(this->ncols) + j];
		}	
	}
	return T;
}

//matrix multiplication function
Matrix mat_mul(Matrix& mat_a, Matrix& mat_b){
	if (mat_a.cols() != mat_b.rows()){
		cerr << "Matrices not compatible for multiplication" << endl; 
		exit(1); 
	}

	Matrix mat_c(mat_a.rows(), mat_b.cols());
	for(int i=0; i<mat_a.rows();i++){
		for (int j=0; j<mat_b.cols(); j++){
			for (int k=0; k<mat_a.cols(); k++)
				mat_c[i][j] += mat_a[i][k] * mat_b[k][j];
		}
	}

	return mat_c;
}

//overloaded addition operator to add matrices
Matrix operator+(Matrix& mat_a, Matrix& mat_b){
	if (mat_a.rows()!=mat_b.rows() && mat_a.cols()!=mat_b.cols()){
	//if(mat_a.size()!=mat_b.size()){
		cerr << "size mismatch" << endl;
		exit(1);
	}
	else {
		Matrix mat_c(mat_a.rows(), mat_a.cols());

		for(int i=0; i<mat_a.rows();i++){
			for (int j=0; j<mat_a.cols(); j++)
				mat_c.set(i,j,mat_a[i][j] + mat_b[i][j]);
		}
		return mat_c;
	}
}

//overloaded subtraction operator to subtract matrices
Matrix operator-(Matrix& mat_a, Matrix& mat_b){
	if (mat_a.rows()!=mat_b.rows() && mat_a.cols()!=mat_b.cols()){
	//if(mat_a.size()!=mat_b.size()){
		cerr << "size mismatch" << endl;
		exit(1);
	}
	else {
		Matrix mat_c(mat_a.rows(), mat_a.cols());
		for(int i=0; i<mat_a.rows();i++){
			for (int j=0; j<mat_a.cols(); j++)
				mat_c.set(i,j,mat_a[i][j] - mat_b[i][j]);
		}
		return mat_c;
	}
}

//overloaded operator for += 
void Matrix::operator+=(Matrix& mat_a){
	if (nrows!=mat_a.rows() && ncols!=mat_a.cols()){
	//if(mat_a.size()!=mat_b.size()){
		cerr << "size mismatch" << endl;
		exit(1);
	}
	else {
		for(int i=0; i<nrows;i++){
			for (int j=0; j<ncols; j++)
				elem[i*ncols + j] += mat_a[i][j];
		}
	}
}

//overloaded operator for -= 
void Matrix::operator-=(Matrix& mat_a){
	if (nrows!=mat_a.rows() && ncols!=mat_a.cols()){
	//if(mat_a.size()!=mat_b.size()){
		cerr << "size mismatch" << endl;
		exit(1);
	}
	else {
		for(int i=0; i<nrows;i++){
			for (int j=0; j<ncols; j++)
				elem[i*ncols + j] -= mat_a[i][j];
		}
	}
}

//overloaded boolean nonequality operator
bool operator!=(Matrix& mat_a, Matrix& mat_b){
	if (mat_a.rows()!=mat_b.rows() || mat_a.cols()!=mat_b.cols()){
		return true;
	}
	else {
		for(int i=0; i<mat_a.rows();i++){
			for (int j=0; j<mat_a.cols(); j++){
				if (mat_a[i][j] != mat_b[i][j])
					return true;
			}
		}
		return false;
	}
}

//overloaded boolean equality operator
bool operator==(Matrix& mat_a, Matrix& mat_b){
	if (mat_a.rows()!=mat_b.rows() || mat_a.cols()!=mat_b.cols()){
		return false;
	}
	else {
		for(int i=0; i<mat_a.rows();i++){
			for (int j=0; j<mat_a.cols(); j++){
				if (mat_a[i][j] != mat_b[i][j])
					return false;
			}
		}
		return true;
	}
}

//output to stream -- not not a part of the class
std::ostream& operator<<(std::ostream &os, Matrix& mat_in){
	
	for (int i = 0; i<mat_in.rows(); ++i){
		os << "(";
		for (int j = 0; j<mat_in.cols(); ++j)
			os << mat_in[i][j] << " ";
		os << "\b)" << endl;
	}	
	return os;
}
